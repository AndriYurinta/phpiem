-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 11, 2017 at 01:58 PM
-- Server version: 5.5.54-MariaDB-1ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sbyfeb`
--

-- --------------------------------------------------------

--
-- Table structure for table `appsettings`
--

CREATE TABLE IF NOT EXISTS `appsettings` (
  `oid` bigint(20) NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) DEFAULT NULL,
  `option_value` longtext,
  `autoload` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `option_name` (`option_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `appsettings`
--

INSERT INTO `appsettings` (`oid`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'version', '1.0.0', 'yes'),
(2, 'appname', 'PHPIEM', 'yes'),
(3, 'mail_confirm_register', 'This is mail register', 'yes'),
(4, 'ci_email_smtp_host', 'smtp.gmail.com', 'yes'),
(5, 'ci_email_smtp_user', 'eko@pradesga.com', 'yes'),
(6, 'ci_email_charset', 'utf-8', 'yes'),
(7, 'ci_email_protocol', 'smtp', 'yes'),
(8, 'ci_email_mailtype', 'html', 'yes'),
(9, 'ci_email_smtp_pass', '12134', 'yes'),
(10, 'ci_email_smtp_timeout', '5', 'yes'),
(11, 'ci_email_smtp_keepalive', 'false', 'yes'),
(12, 'ci_email_smtp_crypto', 'tls', 'yes'),
(13, 'admin_main_menu', 'a:5:{i:0;a:3:{s:5:"label";s:4:"Home";s:4:"icon";s:4:"home";s:5:"child";a:1:{i:0;a:2:{s:5:"label";s:9:"Dashboard";s:3:"url";s:10:"/backpanel";}}}i:1;a:3:{s:5:"label";s:6:"Events";s:4:"icon";s:5:"table";s:5:"child";a:3:{i:0;a:2:{s:5:"label";s:10:"All Events";s:3:"url";s:17:"/backpanel/events";}i:1;a:2:{s:5:"label";s:12:"Create Event";s:3:"url";s:23:"/backpanel/event/create";}i:2;a:2:{s:5:"label";s:4:"Type";s:3:"url";s:27:"/backpanel/event/event-type";}}}i:2;a:3:{s:5:"label";s:5:"Pages";s:4:"icon";s:5:"clone";s:5:"child";a:2:{i:0;a:2:{s:5:"label";s:9:"All Pages";s:3:"url";s:15:"/backpanel/page";}i:1;a:2:{s:5:"label";s:11:"Create Page";s:3:"url";s:22:"/backpanel/page/create";}}}i:3;a:3:{s:5:"label";s:5:"Users";s:4:"icon";s:4:"user";s:5:"child";a:2:{i:0;a:2:{s:5:"label";s:9:"All Users";s:3:"url";s:15:"/backpanel/user";}i:1;a:2:{s:5:"label";s:11:"Create User";s:3:"url";s:22:"/backpanel/user/create";}}}i:4;a:3:{s:5:"label";s:13:"Site Settings";s:4:"icon";s:4:"gear";s:5:"child";a:2:{i:0;a:2:{s:5:"label";s:7:"General";s:3:"url";s:32:"/backpanel/site-settings/general";}i:1;a:2:{s:5:"label";s:5:"Email";s:3:"url";s:30:"/backpanel/site-settings/email";}}}}', 'yes'),
(14, 'admin_event_menu', 'a:5:{i:0;a:3:{s:5:"label";s:10:"Event Home";s:4:"icon";s:4:"home";s:5:"child";a:2:{i:0;a:2:{s:5:"label";s:15:"Event Dashboard";s:3:"url";s:26:"/backpanel/event/[eventid]";}i:1;a:2:{s:5:"label";s:12:"Switch Event";s:3:"url";s:17:"/backpanel/events";}}}i:1;a:3:{s:5:"label";s:12:"Registration";s:4:"icon";s:4:"user";s:5:"child";a:1:{i:0;a:2:{s:5:"label";s:8:"Check-In";s:3:"url";s:34:"/backpanel/event/[eventid]/checkin";}}}i:2;a:3:{s:5:"label";s:8:"Attendee";s:4:"icon";s:5:"users";s:5:"child";a:2:{i:0;a:2:{s:5:"label";s:12:"All Attendee";s:3:"url";s:35:"/backpanel/event/[eventid]/attendee";}i:1;a:2:{s:5:"label";s:12:"Add Attendee";s:3:"url";s:30:"/backpanel/event/[eventid]/new";}}}i:3;a:3:{s:5:"label";s:9:"Feedbacks";s:4:"icon";s:4:"feed";s:5:"child";a:1:{i:0;a:2:{s:5:"label";s:13:"All Feedbacks";s:3:"url";s:35:"/backpanel/event/[eventid]/feedback";}}}i:4;a:3:{s:5:"label";s:8:"Settings";s:4:"icon";s:4:"gear";s:5:"child";a:4:{i:0;a:2:{s:5:"label";s:7:"General";s:3:"url";s:43:"/backpanel/event/[eventid]/settings/general";}i:1;a:2:{s:5:"label";s:5:"Forms";s:3:"url";s:41:"/backpanel/event/[eventid]/settings/forms";}i:2;a:2:{s:5:"label";s:14:"Email Template";s:3:"url";s:50:"/backpanel/event/[eventid]/settings/email-template";}i:3;a:2:{s:5:"label";s:12:"Notification";s:3:"url";s:48:"/backpanel/event/[eventid]/settings/notification";}}}}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `event_create` datetime DEFAULT NULL,
  `event_update` datetime DEFAULT NULL,
  `event_owner` int(11) DEFAULT NULL,
  `event_name` varchar(191) DEFAULT NULL,
  `event_slug` varchar(191) DEFAULT NULL,
  `event_venue` text,
  `event_from` datetime DEFAULT NULL,
  `event_to` datetime DEFAULT NULL,
  `short_desc` text,
  `description` longtext,
  `deleted` tinyint(1) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`eid`),
  UNIQUE KEY `event_slug` (`event_slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`eid`, `event_create`, `event_update`, `event_owner`, `event_name`, `event_slug`, `event_venue`, `event_from`, `event_to`, `short_desc`, `description`, `deleted`, `status`) VALUES
(1, '2017-01-22 20:57:32', '2017-01-22 20:57:32', 1, 'Surabaya Developer Day 2017', 'surabaya-developer-day-2017', 'Dyandra Convention Center (Ex. Gramedia Expo) Surabaya', '2017-02-25 08:00:00', '2017-02-26 16:00:00', 'Surabaya Developer Day 2017\r\nDyandra Convention Center (Ex. Gramedia Expo) Surabaya\r\nJl. Basuki Rahmat No. 105\r\nEmbong Kaliasin, Genteng\r\nSurabaya', '<p>Expo, Seminar and Competition</p>\r\n<p>Surabaya Developer Day 2017<br />Dyandra Convention Center (Ex. Gramedia Expo) Surabaya<br />Jl. Basuki Rahmat No. 105<br />Embong Kaliasin, Genteng<br />Surabaya</p>', 0, 'Waiting Registration');

-- --------------------------------------------------------

--
-- Table structure for table `events_meta`
--

CREATE TABLE IF NOT EXISTS `events_meta` (
  `emid` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `meta_name` text,
  `meta_value` longtext,
  `meta_order` int(6) DEFAULT NULL,
  PRIMARY KEY (`emid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events_type`
--

CREATE TABLE IF NOT EXISTS `events_type` (
  `etid` int(11) NOT NULL AUTO_INCREMENT,
  `et_name` varchar(66) DEFAULT NULL,
  `et_slug` varchar(66) DEFAULT NULL,
  `et_parent` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `status` smallint(2) DEFAULT NULL,
  PRIMARY KEY (`etid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `fbid` bigint(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `suggestion` longtext,
  `deleted` tinyint(1) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`fbid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_meta`
--

CREATE TABLE IF NOT EXISTS `feedback_meta` (
  `fbmid` bigint(20) NOT NULL AUTO_INCREMENT,
  `fbid` int(11) DEFAULT NULL,
  `meta_name` text,
  `meta_value` longtext,
  `meta_order` int(6) DEFAULT NULL,
  PRIMARY KEY (`fbmid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `formgen`
--

CREATE TABLE IF NOT EXISTS `formgen` (
  `fid` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `form_event_id` int(11) DEFAULT NULL,
  `form_owner` int(11) DEFAULT NULL,
  `form_title` varchar(60) DEFAULT NULL,
  `form_class` varchar(60) DEFAULT NULL,
  `form_tag_id` varchar(32) DEFAULT NULL,
  `form_method` varchar(12) DEFAULT NULL,
  `form_action` text,
  `form_success` text,
  `form_failed` text,
  `form_warning` text,
  `deleted` tinyint(1) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `formgen_element`
--

CREATE TABLE IF NOT EXISTS `formgen_element` (
  `elm_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `elm_form_id` int(11) DEFAULT NULL,
  `elm_label` varchar(32) DEFAULT NULL,
  `elm_required` tinyint(1) DEFAULT NULL,
  `elm_uid` varchar(23) DEFAULT NULL,
  `elm_eid` varchar(16) DEFAULT NULL,
  `elm_class` varchar(32) DEFAULT NULL,
  `elm_enum` text,
  `elm_warning` varchar(62) DEFAULT NULL,
  PRIMARY KEY (`elm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `formgen_type`
--

CREATE TABLE IF NOT EXISTS `formgen_type` (
  `ftid` int(11) NOT NULL AUTO_INCREMENT,
  `ft_name` varchar(33) DEFAULT NULL,
  `ft_slug` varchar(33) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `status` smallint(2) DEFAULT NULL,
  PRIMARY KEY (`ftid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `pageid` bigint(20) NOT NULL AUTO_INCREMENT,
  `page_author` int(11) DEFAULT NULL,
  `page_date` datetime DEFAULT NULL,
  `page_title` varchar(40) DEFAULT NULL,
  `page_slug` varchar(40) DEFAULT NULL,
  `page_excerpt` text,
  `page_content` longtext,
  `page_modified` datetime DEFAULT NULL,
  `page_parent` bigint(20) DEFAULT NULL,
  `page_menuorder` int(6) DEFAULT NULL,
  `page_type` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pageid`),
  UNIQUE KEY `page_slug` (`page_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page_meta`
--

CREATE TABLE IF NOT EXISTS `page_meta` (
  `pmid` bigint(22) NOT NULL AUTO_INCREMENT,
  `pageid` bigint(20) DEFAULT NULL,
  `meta_key` text,
  `meta_value` text,
  PRIMARY KEY (`pmid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page_type`
--

CREATE TABLE IF NOT EXISTS `page_type` (
  `typeid` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` text,
  `type_slug` text,
  `type_parent` text,
  `type_status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`typeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `reg_date` datetime DEFAULT NULL,
  `login_name` varchar(20) DEFAULT NULL,
  `password` text,
  `full_name` varchar(20) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `isadmin` tinyint(1) NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `formkey` longtext,
  `deleted` tinyint(1) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `login_name` (`login_name`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `reg_date`, `login_name`, `password`, `full_name`, `email`, `city`, `isadmin`, `isactive`, `formkey`, `deleted`, `status`) VALUES
(1, '2017-01-15 13:24:55', 'admin', '$2y$10$wmUfwiEIx0irRqoLFezP3.PePDJpQ67XmvFnHGStkmFuETyFbK2OO', 'Admin Master', 'eko@pradesga.com', 'Ponorogo', 1, 1, NULL, 0, '1'),
(2, '2017-02-07 04:28:54', 'bagus@droid.dr', NULL, 'Bagus Ilham', 'bagus@droid.dr', 'Ponorogo', 0, 1, NULL, 0, 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `users_meta`
--

CREATE TABLE IF NOT EXISTS `users_meta` (
  `umid` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `meta_name` text,
  `meta_value` longtext,
  `meta_order` int(6) DEFAULT NULL,
  PRIMARY KEY (`umid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users_meta`
--

INSERT INTO `users_meta` (`umid`, `user_id`, `meta_name`, `meta_value`, `meta_order`) VALUES
(1, 2, 'handphone', '879050333453', 2),
(2, 2, 'alamat', 'Jl. Kumbokarno No. 5, Jenes', 1),
(3, 2, 'instansi', 'Tukang Palak', 3),
(4, 2, 'kategori', 'Programmer', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users_order`
--

CREATE TABLE IF NOT EXISTS `users_order` (
  `uoid` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `ticket_code` varchar(32) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `paid_date` datetime DEFAULT NULL,
  `attend_date` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`uoid`),
  UNIQUE KEY `ticket_code` (`ticket_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users_order`
--

INSERT INTO `users_order` (`uoid`, `user_id`, `event_id`, `price`, `ticket_code`, `order_date`, `paid_date`, `attend_date`, `deleted`, `status`) VALUES
(1, 2, 1, 150000, 'gjksk78sd', '2017-02-07 04:30:38', NULL, NULL, 0, 'Proses Pembayaran');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
