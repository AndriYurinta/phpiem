<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Attendeemodel extends CI_Model 
{
	protected $uid;
	
	protected $reg_date;
	protected $login_name;
	protected $complete_name;
	protected $email;
	protected $city;

	protected $isadmin = false;
	protected $isactive = true;
	protected $deleted = false;
	protected $status = 1;
	protected $usermeta = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function GetAll()
	{
		$evid = $this->uri->segment(3);

		$this->db->select('users.uid AS "ID", users_order.ticket_code AS "Ticket Code", users.full_name AS "Full Name", DATE_FORMAT(users.reg_date, "%e %M %Y") AS "Register Date", users.email AS Email, users_order.status AS Status');
		$this->db->from('users');
		$this->db->join('users_order', 'users_order.user_id = users.uid', 'left');
		$this->db->where('users_order.event_id', $evid);
		$this->db->where('users_order.deleted', false);
		$this->db->where_not_in('users_order.status', 'Batal');


		$qry = $this->db->get();
		return $qry->result_array();
	}

	public function GetMetaAttendee($uid, $name = '')
	{
		$results = [];

		$this->db->select('umid, meta_name, meta_value');
		$this->db->from('users_meta');
		$this->db->where('uid', $uid);
		if ( $name != '' ) {
			$this->db->where('meta_name', $name);
			$qry = $this->db->get();
			$results = $qry->row();
		} else {
			$qry = $this->db->get();
			$results = $qry->result_array();
		}

		return $results;
	}

	public function AddNewAttendee()
	{
		if ( $this->CekEmail() )
			return 'error: email exists!';

		$this->db->trans_begin();

		$this->usermeta = array(
			'handphone'	=> $this->input->post('nohp'),
			'alamat' 	=> $this->input->post('alamat'),
			'instansi'	=> $this->input->post('instansi'),
			'kategori'	=> $this->input->post('kategori')
		);

		$datuser = array(
			'reg_date' 		=> date('Y-m-d H:i:s'),
			'login_name' 	=> $this->input->post('email'),
			'complete_name' => $this->input->post('nama'),
			'email' 		=> $this->input->post('email'),
			'city' 			=> $this->input->post('kota'),
			'isactive'		=> true,
			'isadmin' 		=> false,
			'deleted'		=> false,
			'status'		=> 'Aktif'
		);

		$this->db->insert('users', $datuser);
		$lid = $this->db->insert_id();

		$i = 0;
		foreach ($this->usermeta as $mk => $mv) 
		{
			$mdata = array();
			
			$mdata['user_id'] 		= $lid;
			$mdata['meta_name'] 	= $mk;
			$mdata['meta_value'] 	= $mv;
			$mdata['meta_order'] 	= $i;

			$this->db->insert('users_meta', $mdata);
			$i++;
		}

		$order = array(
			'user_id' => $lid,
			'event_id' => 1,
			'order_date' => date('Y-m-d H:i:s'),
			'deleted' => false,
			'status' => 1
		);

		if($this->usermeta['kategori'] === 'student')
		{
			$order['price'] = 75000;
		} else {
			$order['price'] = 100000;
		}

		$this->db->insert('users_order', $order);

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return 'error: database error!';
		}
		else
		{
			$this->db->trans_commit();
			return 'success: data has been saved!';
		}
	}

	public function CekEmail()
	{
		$qry = $this->db->get_where('users', array('email' => $this->input->post('email')));
		return $qry->num_rows() > 0;
	}

	public function forscanner()
	{
		$evid = $this->uri->segment(3);

		$this->db->select('users.uid AS "ID"');
		$this->db->select('users.full_name AS "Full Name"');
		$this->db->select('DATE_FORMAT(users.reg_date, "%e %M %Y") AS "Register Date"');
		$this->db->select('users.email AS Email');
		$this->db->select('users_order.status AS Status');

		$this->db->from('users');
		$this->db->join('users_order', 'users_order.user_id = users.uid', 'left');
		$this->db->where('users_order.event_id', $evid);
		$this->db->where('users_order.deleted', false);
	}

	public function updatescanner()
	{
		$object = [
			'user_id' 		=> $this->input->post('user_id'),
			'event_id' 		=> $this->input->post('event_id'),
			'ticket_code' 	=> $this->input->post('ticket_code'),
			'attend_date' 	=> date('Y-m-d H:i:s'),
			'status' 		=> 'Hadir'
		];
		
		$this->db->where('oid', $this->input->post('oid'));
		$this->db->update('users_order', $object);
	}

	public function generate_ticket($kodeticket)
	{
		$this->db->select('events.event_name, 
							events.event_venue,
							DATE_FORMAT(events.event_from, "%M %e, %Y on %H.%i WIB") AS event_from,
							events.description,
							users.full_name,
							users.email,
							users.city,
							users_order.ticket_code');

		$this->db->from('users_order');
		$this->db->join('users', 'users_order.user_id = users.uid', 'left');
		$this->db->join('events', 'users_order.event_id = events.eid', 'right');
		$this->db->where('users_order.ticket_code', strtoupper($kodeticket));
		$this->db->where('users_order.deleted', false);
		$this->db->where_not_in('users_order.status', 'Batal');

		$qry = $this->db->get();
		$res = $qry->result_array();
		if ( $res != null )
			return $res[0];
		else
			return;
	}
}